Shiro Demo

包括以下 5 个 Demo：

1. hello - Shiro 的 Hello World 示例

2. webapp - Shiro 的 Web 应用示例

3. spring - 在 Spring Web 应用中使用 Shiro

4. servlet - 在普通的 Web 应用中使用 Smart Security

5. smart - 在 Smart Web 应用中使用 Smart Security